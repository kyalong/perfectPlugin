var sx = 0,
  sy = 0,
  mx = 0,
  my = 0,
  ex = 0,
  ey = 0,
  sleft = 0,
  stop = 0
Component({
  properties: {
    items: Array,
    allowRemove: {
      type: Boolean,
      value: true
    },
    allowChoose: {
      type: Boolean,
      value: true
    }
  },
  /**
   * 页面的初始数据
   */
  data: {
    hidden: true,
    flag: false,
    disabled: true,
    elements: []
  },
  methods: {
    /**
     * 生命周期函数--监听页面加载
     */
    longpress(e) {
      // console.log(e)
      this.setData({
        hidden: false,
        flag: true
      })
    },
    //触摸开始
    touchstart: function (e) {
      //  console.log(e)
      sx = e.changedTouches[0].clientX
      sy = e.changedTouches[0].clientY
      sleft = e.currentTarget.offsetLeft - 10
      stop = e.currentTarget.offsetTop - 10
      this.setData({
        beginIndex: e.currentTarget.dataset.index,
        selectedItem: e.currentTarget.dataset,
        left: sleft,
        top: stop,
      })
    },
    //
    touchmove(e) {
      //  console.log(e)
      if (!this.data.flag) {
        return;
      }

      mx = e.changedTouches[0].clientX
      my = e.changedTouches[0].clientY
      let left = sleft + mx - sx
      let top = stop + my - sy
      this.setData({
        left,
        top
      })
    },
    //触摸结束
    touchend(e) {
      let that = this
      //  console.log(e)
      if (!this.data.flag) {
        return;
      }

      var query = this.createSelectorQuery();
      var nodesRef = query.selectAll(".item");
      nodesRef.fields({
        dataset: true,
        rect: true

      }, (result) => {

        const x = e.changedTouches[0].clientX
        const y = e.changedTouches[0].clientY

        const list = result;
        let items = that.data.items
        for (var j = 0; j < list.length - 1; j++) {
          const item = list[j];
          if (x > item.left && x < item.right && y > item.top && y < item.bottom) {
            const endIndex = item.dataset.index;
            const beginIndex = that.data.beginIndex;
            this.arraymove(items, beginIndex, endIndex)
            console.log(items)
            that.setData({
              items
            })
            this.triggerEvent('updateDataAction', items)
          }
        }
        that.setData({
          hidden: true,
          flag: false
        })


      }).exec()

    },
    arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    },
    chooseItem(e) {
      const that = this;
    wx.chooseImage({
      count: 9,
      success:res=>{
        //console.log(res)
        this.setData({
          items:res.tempFilePaths
        })
      }
    })

    },
    removeItem(e) {
      console.log(e);
      let items = this.data.items;
      const index = e.currentTarget.dataset.index
      items.splice(index, 1);
      console.log(items)
      this.setData({
        items
      });

    },
    tapItem(e) {
      console.log('ss', e)
      this.triggerEvent('tapItemAction', e.currentTarget.dataset.index)
    }
  },

})