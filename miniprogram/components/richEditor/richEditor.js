Component({
  /**
   * 组件的属性列表
   */
  properties: {
    nodes: String,
    insertImg: {
      type: Boolean,
      value:true
    },
  },
  
  observers: {
    "nodes": function (nodes) {
      if (this.nodes !== nodes) {

        const that = this
        this.createSelectorQuery().select('#editor').context(function (res) {
          that.editorCtx = res.context
          that.editorCtx.setContents({
            html: nodes,
            success:  (res)=> {
              wx.pageScrollTo({
                scrollTop: 0,
                duration: 3
              });
            },
          })
          // that.editorCtx.blur()
        }).exec()

      }
      //this.triggerEvent('change', nodes);
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    formats: {},
    isEditing:false
  },


  /**
   * 组件的方法列表
   */
  methods: {
    onEditorReady() {
      const that = this
      this.createSelectorQuery().select('#editor').context(function (res) {
        that.editorCtx = res.context
        that.editorCtx.setContents({
          html: that.data.nodes,
          success:  (res)=> {
            wx.pageScrollTo({
              scrollTop: 0,
              duration: 3
            });
          },
        })

      
      }).exec()

    },

    format(e) {
      let {
        name,
        value
      } = e.target.dataset
      if (!name) return
      // console.log('format', name, value)
      this.editorCtx.format(name, value)

    },
    onStatusChange(e) {
      const formats = e.detail
      console.log(formats)
      this.setData({
        formats
      })
    },
    insertDivider() {
      this.editorCtx.insertDivider({
        success: function () {
          console.log('insert divider success')
        }
      })
    },
    clear() {
      this.editorCtx.clear({
        success: function (res) {
          console.log("clear success")
        }
      })
    },
    removeFormat() {
      this.editorCtx.removeFormat()
    },
    insertDate() {
      const date = new Date()
      const formatDate = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
      this.editorCtx.insertText({
        text: formatDate
      })
    },

    insertImage() {
      // this.triggerEvent('insertImg', this.editorCtx)
      const that = this;
      wx.navigateTo({
        url: '/pages/admin/gallary/gallary',
        success: res => {
          res.eventChannel.on('setImages', items => {
            for (let i = 0; i < items.length; i++) {
              let tempFile = items[i];
              that.editorCtx.insertImage({
                src: tempFile.src,
                // data: {
                //   id: 'img'+i,
                //   role: 'img'+i
                // },
                width: '100%',
                success: function () {
                  console.log('insert image success')
                  that.getNode();
                }
              })
            }
          });
          res.eventChannel.emit('setImages', {
            count: 9
          });
        }
      })
    },

  
    onChange(e) {
       console.log(e.detail)
      this.data.nodes = e.detail.html;
    },
    onFocus(e){
      console.log('focus')
 
      this.setData({
        isEditing: true 
      })
      this.triggerEvent('isEditing', {
        isEditing: true
      })
    },
    onBlur(e){
      console.log('blur')
      this.editorCtx.blur()
      this.triggerEvent('isEditing', {
        isEditing: false
      })
      this.setData({
        isEditing: false 
      })
    },
    getNode() {
      this.editorCtx.getContents({
        success: (res) => {
          console.log('执行获取editor内容')
          // this.triggerEvent('getNode', res.html)
          this.setData({
            nodes: res.html
          })
        },
        fail: (res) => {
          console.log("fail：", res);
        }
      });
    },
  }
})