var startY, endY, moveY, panelHeight
Component({
  properties: {
    selectedcolor: {
      type: String,
      value: 'rgba(255,0,0,1)',
      observer: function (n, o) {   
        this.onInit(n)
      }
    },
    maskClosable: {
      type: Boolean,
      value: true
    },
    mask: {
      type: Boolean,
      value: true
    },
    show: {
      type: Boolean,
      value: false,
      observer: function (newv, oldv) {
        if (newv) {
          this.setData({
            moveY: 0
          })
        }
      }
    },
  },
  data: {
    transparentBg64: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAABG2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS41LjAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+Gkqr6gAAAYJpQ0NQc1JHQiBJRUM2MTk2Ni0yLjEAACiRdZG7SwNBEIe/xPeLKFpYWASJFqISIwRtLBJ8gVrECL6a5PISknjcJYjYCrYBBdHGV6F/gbaCtSAoiiB2grWijco5lwskiJlldr797c6wOwv2YFJJ6ZVuSKUzWmDc55xfWHTWvFBNC3V00BNSdHV6dixIWfu8x2bG2z6zVvlz/1pDJKorYKsVHlFULSM8ITy1llFN3hFuUxKhiPCZcK8mFxS+M/Wwxa8mxy3+NlkLBvxgbxZ2xks4XMJKQksJy8txpZJZpXAf8yWN0fTcrMRO8Q50Aozjw8kko/jxMsCwzF768NAvK8rku/P5M6xKriKzyjoaK8RJkKFX1KxUj0qMiR6VkWTd7P/fvuqxQY9VvdEHVc+G8d4FNdvwkzOMryPD+DmGiie4TBfzVw9h6EP0XFFzHYBjE86vilp4Fy62oP1RDWmhvFQhbo/F4O0Umhag9Qbql6yeFfY5eYDghnzVNeztQ7ecdyz/AnIYZ+vRrQP9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAKElEQVQYlWO8e/cuAxJQUlJC5jIx4AU0lWb8//8/Mv/evXt0s5uANADPtAineFRBKgAAAABJRU5ErkJggg==',
  },
  lifetimes: {
    ready() {
      this.onInit(this.data.selectedcolor)
    },
  },
  methods: {
    onInit(selectedcolor) {
      // for(let i=0;i<360;i++)
      // {
      //   console.log(this.hsv2rgb(i, 100, 100,1))
      // }
  
      // let {
      //   selectedcolor
      // } = this.data,
      selectedcolor = this.hex2rgba(selectedcolor)
      let rgba = this.color2rgba(selectedcolor)
      let hsva = this.rgb2hsv(rgba.r, rgba.g, rgba.b, rgba.a)
  
      let hueColor = this.hsv2rgb(hsva.h, 100, 100, 1)
      // hueColor = `rgba(${hueColor.r},${hueColor.g},${hueColor.b},${hueColor.a})`
  
      const $ = this.createSelectorQuery()
      const target = $.select('.color-palette')
      target.boundingClientRect()
      $.exec((res) => {
        const rect = res[0]
        this.rect = rect
        if (rect) {

          this.setData({
            x: Math.round(hsva.s * rect.width / 100 - 14),
            y: Math.round((100 - hsva.v) * rect.height / 100 - 14),
            // colors,
            hsva,
            rgba,
            hueColor,
          })

        }
      })
    },
    onStart(e) {
      let {
        clientX,
        clientY
      } = e.touches[0],
        x = Math.floor(clientX - this.rect.left - 14),
        y = Math.floor(clientY - this.rect.top - 14)
        console.log(e.touches[0],x,y)
      if (x < -14) {
        x = -14
      }
      if (y < -14) {
        y = -14
      }
      if (x >= this.rect.width - 14) {
        x = Math.floor(this.rect.width - 14)
      }
      if (y >= this.rect.height - 14) {
        y = Math.floor(this.rect.height - 14)
      }
      let s = Math.round((x + 14) / this.rect.width * 100),
        v = Math.round(100 - (y + 14) / this.rect.height * 100),
        hsva = this.data.hsva
      hsva.s = s;
      hsva.v = v;
      let rgbaColor = this.hsv2rgb(hsva.h, s, v, hsva.a)
      let rgba=this.color2rgba(rgbaColor)
      this.setData({
        x,
        y,
        hsva,
        rgba,
        selectedcolor: rgbaColor
      })

    },
    onMove(e) {
      this.onStart(e)
     
    },
    onEnd() {
      this.triggerEvent('changeColor', {
        color: this.data.selectedcolor
      })
    },
    changeHue(e) {
      let hueDeg = e.detail.value
      let hsva = this.data.hsva
      hsva.h = hueDeg
      let hueColor = this.hsv2rgb(hsva.h, 100, 100, 1)
     // console.log(hueColor)  
      let rgbaColor = this.hsv2rgb(hsva.h, hsva.s, hsva.v, hsva.a)
      let rgba=this.color2rgba(rgbaColor)
      this.setData({
        hueColor,
        hsva,
        rgba,
        selectedcolor: rgbaColor
      })

    },
    changeAlpha(e) {
      let a = e.detail.value.toFixed(2)
      this.setData({
        'rgba.a': a,
        'hsva.a': a,
        selectedcolor: `rgba(${this.data.rgba.r},${this.data.rgba.g},${this.data.rgba.b},${this.data.rgba.a})`
      })

    },
    close: function close(e) {
      if (this.data.maskClosable) {
        this.setData({
          show: false,
          moveY: '100vh'
        });
        this.triggerEvent('close');
      }
    },
    panelMoveStart(e) {
   
      startY = e.touches[0].pageY;
      panelHeight = wx.getSystemInfoSync().windowHeight - e.currentTarget.offsetTop
    },
    panelMove(e) {
      if (startY > e.currentTarget.offsetTop && startY < e.currentTarget.offsetTop + Math.round(wx.getSystemInfoSync().windowWidth * 0.075)) {
        endY = e.touches[0].pageY;
        moveY = endY - startY

        if (moveY > 0 && moveY <= panelHeight) {
          this.setData({
            moveY: moveY + 'px'
          })
        } else if (moveY > panelHeight) {
          this.setData({
            show: false,
            moveY: '100vh'
          })
        } else {
          return
        }
      }
    
    },
    panelMoveEnd(e) {
   
      if (moveY <= panelHeight) {
        this.setData({
          moveY: 0
        })
      }

    },
    preventdefault: function () {

    },
    hex2rgba(color) { //#ffffff
      //十六进制颜色值的正则表达式
      var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
      var sColor = color.toLowerCase();
      if (sColor && reg.test(sColor)) {
        if (sColor.length === 4) {
          var sColorNew = "#";
          for (var i = 1; i < 4; i += 1) {
            sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
          }
          sColor = sColorNew;
        }
        //处理六位的颜色值
        var sColorChange = [];
        for (var i = 1; i < 7; i += 2) {
          sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
        }
        return "rgba(" + sColorChange.join(",") + ",1)";
      } else {
        return sColor;
      }

    },
    color2rgba(color) {
      let rgbaColor = color.split(','),
        rgba = {};
    
      if (rgbaColor[0].split('(')[0] == 'rgba') {
        rgba.r = parseInt(rgbaColor[0].split('(')[1]);
        rgba.g = parseInt(rgbaColor[1]);
        rgba.b = parseInt(rgbaColor[2]);
        rgba.a = parseFloat(rgbaColor[3].split(')')[0]).toFixed(2);
      } else {
        rgba.r = parseInt(rgbaColor[0].split('(')[1]);
        rgba.g = parseInt(rgbaColor[1]);
        rgba.b = parseInt(rgbaColor[2].split(')')[0]);
        rgba.a = 1
      }
      return rgba
    },
    hsv2rgb: function (h, s, v,a) {
      let hsv_h = (h / 360).toFixed(2);
      let hsv_s = (s / 100).toFixed(2);
      let hsv_v = (v / 100).toFixed(2);

      var i = Math.floor(hsv_h * 6);
      var f = hsv_h * 6 - i;
      var p = hsv_v * (1 - hsv_s);
      var q = hsv_v * (1 - f * hsv_s);
      var t = hsv_v * (1 - (1 - f) * hsv_s);

      var rgb_r = 0,
        rgb_g = 0,
        rgb_b = 0;
      switch (i % 6) {
        case 0:
          rgb_r = hsv_v;
          rgb_g = t;
          rgb_b = p;
          break;
        case 1:
          rgb_r = q;
          rgb_g = hsv_v;
          rgb_b = p;
          break;
        case 2:
          rgb_r = p;
          rgb_g = hsv_v;
          rgb_b = t;
          break;
        case 3:
          rgb_r = p;
          rgb_g = q;
          rgb_b = hsv_v;
          break;
        case 4:
          rgb_r = t;
          rgb_g = p;
          rgb_b = hsv_v;
          break;
        case 5:
          rgb_r = hsv_v, rgb_g = p, rgb_b = q;
          break;
      }

      return 'rgba(' + (Math.floor(rgb_r * 255) + "," + Math.floor(rgb_g * 255) + "," + Math.floor(rgb_b * 255)) + ','+a+')';
    },  
    rgb2hsv: function (R, G, B, a) {
      // let rgb = color.split(',');
      // let R = parseInt(rgb[0].split('(')[1]);
      // let G = parseInt(rgb[1]);
      // let B = parseInt(rgb[2].split(')')[0]);

      let hsv_red = R / 255,
        hsv_green = G / 255,
        hsv_blue = B / 255;
      let hsv_max = Math.max(hsv_red, hsv_green, hsv_blue),
        hsv_min = Math.min(hsv_red, hsv_green, hsv_blue);
      let hsv_h, hsv_s, hsv_v = hsv_max;

      let hsv_d = hsv_max - hsv_min;
      hsv_s = hsv_max == 0 ? 0 : hsv_d / hsv_max;

      if (hsv_max == hsv_min) hsv_h = 0;
      else {
        switch (hsv_max) {
          case hsv_red:
            hsv_h = (hsv_green - hsv_blue) / hsv_d + (hsv_green < hsv_blue ? 6 : 0);
            break;
          case hsv_green:
            hsv_h = (hsv_blue - hsv_red) / hsv_d + 2;
            break;
          case hsv_blue:
            hsv_h = (hsv_red - hsv_green) / hsv_d + 4;
            break;
        }
        hsv_h /= 6;
      }
      return {
        h: (hsv_h * 360).toFixed(),
        s: (hsv_s * 100).toFixed(),
        v: (hsv_v * 100).toFixed(),
        a: a
      }
    },
  }
})