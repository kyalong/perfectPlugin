# 一、图片编辑组件
图片旋转、剪裁、涂鸦。
## 裁剪效果预览图
![演示图](https://images.gitee.com/uploads/images/2021/0810/102244_66508cb6_1863749.gif "WeChat_20210810092707.gif")
## 体验路径：DIY头像--》图片--剪裁
![输入图片说明](https://images.gitee.com/uploads/images/2021/0810/102419_f2cfa318_1863749.jpeg "微信图片_20210517111505.jpg")
# 二、取色器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0810/110416_c4f182c6_1863749.jpeg "微信图片_20210810110330.jpg")
# 三、图片组拖拽排序
![输入图片说明](https://images.gitee.com/uploads/images/2021/0810/110427_25c1fff2_1863749.jpeg "微信图片_20210810110319.jpg")
# 四、图文混排编辑器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0810/110439_03823c61_1863749.jpeg "微信图片_20210810110314.jpg")